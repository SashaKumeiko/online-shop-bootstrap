// Гамбургер
$('.top-menu-items__btn').click(function(event) {
    event.preventDefault();
    if($('.top-menu-items__btn').hasClass('active')) {
        $('.top-menu-items__btn').removeClass('active');
    } else {
        $('.top-menu-items__btn').addClass('active');
    }
});

// Промотування до секції
$('a[data-name]').click(function (event) {
    event.preventDefault();
    let $id = $(`section[data-name="${event.currentTarget.dataset["name"]}"]`);
    $('html').animate({ scrollTop: $id.offset().top }, 500);
});